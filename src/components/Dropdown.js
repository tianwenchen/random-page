import { Arrow } from "@/components/Arrow";
import { Button } from "@/components/Button"
import { useEffect, useRef, useState } from "react"

const classes = {
  container: "relative inline-block w-80",
  wrapper: "z-10 mt-2 bg-white divide-y divide-gray-100 rounded-lg shadow-lg w-80 dark:bg-gray-700 absolute",
  list: "py-2 text-sm text-gray-700 dark:text-gray-200",
  item: "block px-4 py-2 hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white",
}

export const Dropdown = ({ options, onSelect, selected: selectedOption }) => {
  const selected = selectedOption || options[0];
  const [toggle, setToggle] = useState(false)
  const ref = useRef(null)

  const onToggle = () => setToggle(!toggle)
  const select = option => () => onToggle(onSelect(option))
  const closeOnBlur = (e) => { if (!ref?.current?.contains(e.target)) { setToggle(false) } }

  useEffect(() => {
    document.addEventListener('mousedown', closeOnBlur)
  }, [ref])

  return (
    <div ref={ref} className={classes.container}>
      <Button className="w-80 text-left" onClick={onToggle} theme="light">
        {selected.title} <Arrow rotate={toggle} />
      </Button>
      {
        toggle && (
          <div className={classes.wrapper}>
            <ul className={classes.list}>
              {
                options.map(option => (
                  <li key={option.title}>
                    <a href="#" className={classes.item} onClick={select(option)}>{option.title}</a>
                  </li>
                ))
              }
            </ul>
          </div>
        )
      }
    </div>
  )
}
