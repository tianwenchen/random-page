const defaultClassName = "text-blue-800 hover:text-blue-500"

export const Link = ({ className, children, href, ...props }) => {
  const url = href || children
  const openInNewPage = url && !(url.startsWith('#') || url.startsWith('javascript:') ) && typeof (props.target) === "undefined"
  if (openInNewPage) { props.target = "_blank" }

  return (
    <a href={href || children} className={className || defaultClassName} {...props}>{ children }</a>
  )
}
