import { Link } from "@/components/Link"
import { groupBy } from "lodash"
import { useState } from "react"

export const Visited = ({ urls: sourceUrls }) => {
  const [toggle, setToggle] = useState(false)
  const onToggle = () => setToggle(!toggle)

  if (sourceUrls.length === 0) return

  const groups = groupBy(sourceUrls, url => new URL(url).hostname)

  return (
    <>
      <p className="mt-4">
        <b>{sourceUrls.length} pages visited</b>
        {" "}
        <a href="#" onClick={onToggle}>({toggle ? "hide" : "show"})</a>
      </p>
      {
        toggle && (
          <ul className="pl-2 mt-2">
            {
              Object.entries(groups).map(([group, urls]) => (
                <li key={group}>
                  {group}
                  <ul className="pl-2 list-disc list-inside marker:text-gray-300">
                    {
                      urls.map(url => (
                        <li key={url}>
                          <Link href={url} target="_blank">{url}</Link>
                        </li>
                      ))
                    }
                  </ul>
                </li>
              ))
            }
          </ul>
        )
      }
    </>
  )
}
