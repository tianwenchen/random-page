import localforage from "localforage"

const SITEMAP_KEY = "randomPage.sitemap"
const VISITED_KEY = "randomPage.visited"

export const loadVisitedUrls = async () => {
  return await localforage.getItem(VISITED_KEY) || []
}

export const saveVisited = async (visited) => {
  await localforage.setItem(VISITED_KEY, visited || [])
}
