import RootLayout from "@/app/layout"

const App = ({ children, Component, ...options }) => {
  return (
    <RootLayout>
      <Component { ...options.pageProps }>{children}</Component>
    </RootLayout>
  )
}

export default App
