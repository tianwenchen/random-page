import { concat, flatten, pullAll, sample } from "lodash"
import { useEffect, useState } from "react"
import { parseStringPromise } from "xml2js"

import { Button } from "@/components/Button"
import { Dropdown } from "@/components/Dropdown"
import { Link } from "@/components/Link"
import { Section } from "@/components/Section"
import { Visited } from "@/components/Visited"
import { loadVisitedUrls, saveVisited } from "@/services/Sites"

const websites = ["https://handbook.gitlab.com", "https://docs.gitlab.com"]

const Home = ({ sitemapUrls: sitemapUrls }) => {
  const [website, setWebsite] = useState(null)
  const [url, setUrl] = useState(null)
  const [visitedUrls, setVisitedUrls] = useState([])

  const allWebsiteUrls = flatten(Object.values(sitemapUrls))
  const websiteUrls = website === null ? allWebsiteUrls : sitemapUrls[website]
  const websiteOptions = [{
    website: null,
    title: `All (${allWebsiteUrls.length})`
  }].concat(websites.map(website => ({
    website,
    title: `${website} (${sitemapUrls[website].length})`
  })))
  const selectedWebsiteOption = websiteOptions.find(({ website: value }) => value === website)

  const urls = pullAll(websiteUrls, visitedUrls)

  const onLucky = () => setUrl(sample(urls))
  const onOpen = () => {
    if (!url) return

    window.open(url, '_blank')
    updateVisited(concat(visitedUrls, url))
    setUrl(null)
  }
  const onClear = () => {
    updateVisited([])
  }
  const onSelectWebsite = option => setWebsite(option.website)

  const updateVisited = (visited) => {
    setVisitedUrls(visited)
    saveVisited(visited)
  }

  useEffect(() => {
    const load = async () => setVisitedUrls(await loadVisitedUrls())
    load()
  }, [])

  return (
    <>
      <Section className="text-2xl">
        Pick a random page!
      </Section>
      <Section className="mb-auto">
        <Dropdown options={websiteOptions} selected={selectedWebsiteOption} onSelect={onSelectWebsite} />
        {" "}
        <Button onClick={onLucky}>I&lsquo;m feeling lucky</Button>
        {" "}
        <Button onClick={onClear} theme="secondary">Clear</Button>
        {url && (
          <Section>
            <a href="#" onClick={onOpen}>{url}</a>
          </Section>
        )}
        <Visited urls={visitedUrls} />
      </Section>
      <Section>
        <Link href="https://gitlab.com/tianwenchen/random-page">Repo on Gitlab</Link>.
      </Section>
    </>
  )
}

export default Home

export const getStaticProps = async (context) => {
  // Fetch data from sitemaps
  const sitemapUrls = {}
  const load = async (baseUrl) => {
    const requestedUrl = `${baseUrl}/sitemap.xml`
    const res = await fetch(requestedUrl)
    const text = await res.text()
    const data = await parseStringPromise(text)
    const urls = data.urlset.url.map(({ loc: [url] }) => url)
    sitemapUrls[baseUrl] = urls
  }
  await Promise.all(websites.map(load))

  // Pass data to the page via props
  return { props: { sitemapUrls } }
}
